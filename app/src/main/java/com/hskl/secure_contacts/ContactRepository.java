package com.hskl.secure_contacts;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

// Das Repository zur Datenbank
//Hier werden die anfragen an die Datenbank in AsyncTasks verpackt
//
public class ContactRepository {

    private ContactDao mContactDao;
    private LiveData<List<Contact>> mAllContacts;
    private LiveData<Contact> mProfile;

    ContactRepository(Application application){
        ContactsDatabase db = ContactsDatabase.getInstance(application);
        mContactDao = db.contactDao();
        mAllContacts = mContactDao.getAllContacts();
        mProfile = mContactDao.getProfile();
    }

    LiveData<List<Contact>> getAllContacts(){
        return mAllContacts;
    }

    LiveData<List<Contact>> getContactsById(int... ids){
        return mContactDao.loadAllByIds(ids);
    }

    public LiveData<Contact> getProfile(){
        return mProfile;
    }

    public void insertAll(Contact... contacts){
        new insertAllAsyncTask(mContactDao).execute(contacts);
    }

    private static class insertAllAsyncTask extends AsyncTask<Contact, Void, Void> {

        private ContactDao mAsyncTaskDao;

        insertAllAsyncTask(ContactDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Contact... params) {
            mAsyncTaskDao.insert(params);
            return null;
        }
    }

    public void deleteAll(){
        new deleteAllAsyncTask(mContactDao).execute();
    }

    private static class deleteAllAsyncTask extends AsyncTask<Void, Void, Void> {

        private ContactDao mAsyncTaskDao;

        deleteAllAsyncTask(ContactDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }

    public void delete(int contactID){
        new deleteAsyncTask(mContactDao).execute(contactID);
    }

    private static class deleteAsyncTask extends AsyncTask<Integer, Void, Void>{

        private ContactDao mAsyncTaskDao;

        deleteAsyncTask(ContactDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Integer... contactID) {
            mAsyncTaskDao.delete(contactID[0]);
            return null;
        }
    }

    public void updateContact(Contact contact){
        new updateContactAsyncTask(mContactDao).execute(contact);
    }

    private static class updateContactAsyncTask extends AsyncTask<Contact, Void, Void>{

        private ContactDao mAsyncTaskDao;

        updateContactAsyncTask(ContactDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Contact... contacts) {
            mAsyncTaskDao.updateContact(contacts[0]);
            return null;
        }
    }

}
