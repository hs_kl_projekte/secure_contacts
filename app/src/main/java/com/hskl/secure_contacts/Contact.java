package com.hskl.secure_contacts;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity(tableName = "contacts")
public class Contact implements Parcelable {


    @PrimaryKey(autoGenerate = true)
    private int mContactID;

    @ColumnInfo(name = "isProfile")
    private boolean isProfile = false;

    @ColumnInfo(name = "name")
    private String mName;
    @ColumnInfo(name = "active_phone_number")
    private int mActivePhoneNumber = 0;
    @ColumnInfo(name = "phone_numbers")
    private List<String> mPhoneNumbers;
    @ColumnInfo(name = "email_address")
    private String mEmail;

    public Contact(String name, String email, List<String> phoneNumbers){
        mName = name;
        mPhoneNumbers = phoneNumbers;
        mEmail = email;
    }

    //Konstruktor
    protected Contact(Parcel in) {
        mContactID = in.readInt();
        isProfile = in.readByte() != 0;
        mName = in.readString();
        mActivePhoneNumber = in.readInt();
        mPhoneNumbers = in.createStringArrayList();
        mEmail = in.readString();

    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    //Getter, Setter Methoden für Kontaktinformationen
    public int getContactID(){
        return mContactID;
    }

    public void setContactID(int mContactID) {
        this.mContactID = mContactID;
    }

    public String getName(){
        return mName;
    }

    public void setName(String name){
        mName = name;
    }

    public String getFavouritePhoneNumber() {
        return mPhoneNumbers.get(mActivePhoneNumber);
    }

    public List<String> getPhoneNumbers(){
        return mPhoneNumbers;
    }

    public void addPhoneNumber(String phoneNumber){
        mPhoneNumbers.add(phoneNumber);
    }

    public void setActivePhoneNumber(int activeNumber){
        mActivePhoneNumber = activeNumber;
    }

    public int getActivePhoneNumber() {
        return mActivePhoneNumber;
    }

    public String getEmail(){
        return mEmail;
    }

    public void setEmail(String email){
        mEmail = email;
    }

    public boolean isProfile() {
        return isProfile;
    }

    public void setProfile(boolean profile) {
        isProfile = profile;
    }

    //Vergleicht 2 Kontakte auf Gleichheit bezüglich Name, Email, Telefonnummern
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact contact = (Contact) o;
        if (mName.equals(contact.getName()) && mEmail.equals(contact.getEmail()) && mPhoneNumbers.size() == contact.getPhoneNumbers().size()){
            for (int i = 0; i < mPhoneNumbers.size(); i++) {
                if (!mPhoneNumbers.get(i).equals(contact.getPhoneNumbers().get(i)))
                    return false;
            }
             return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mContactID, mName, mPhoneNumbers, mEmail);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mContactID);
        parcel.writeByte((byte) (isProfile ? 1 : 0));
        parcel.writeString(mName);
        parcel.writeInt(mActivePhoneNumber);
        parcel.writeStringList(mPhoneNumbers);
        parcel.writeString(mEmail);
    }


    public void setPhoneNumbers(List<String> numbers) {
        mPhoneNumbers = numbers;
    }
}
