package com.hskl.secure_contacts;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.ContactsContract;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

//Klasse die dafür verantwortlich ist Kontaktdaten aus Aundroid zu importieren
public class ContactManager{

    private static int CONTACT_PERMISSION_CODE = 1;

    private ContactManager(){}


    //Sended eine Anfrage an den Nutzer um die Berechtigung zu bekommen die Kontaktdaten auslesen zu dürfen
    protected static void requestContactPermission(Context context){
        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_CONTACTS}, ContactManager.CONTACT_PERMISSION_CODE);
    }

    //Liest die Kontaktliste aus der Standard Android App aus und Speichert sie in der Datenbank
    protected static void importContacts(Context context, ContactViewModel contactViewModel, ContactAdapter contactAapter){
        ContentResolver contentResolver =  context.getContentResolver();

        //Anfrage die alle Kontage in einem Cursor Objekt hinterlegt das benutzt wird um auf einzelne Kontakte zuzugreifen
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);

        List<Contact> contactList = new ArrayList<>();
        if(cursor.getCount() > 0){

            //Schleife zum Auslesen von Daten(ID, Name, Telefonnummern, Email) der einzelnen Kontakte
            while(cursor.moveToNext()){
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                //Emails auslesen
                int hasEmail = 1;
                List<String> emails = new ArrayList<>();
                if (hasEmail > 0){
                    Cursor cursor3 = contentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                            new String[] {id},
                            null);
                    while(cursor3.moveToNext()){
                        String email = cursor3.getString(cursor3.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        emails.add(email);
                    }
                }
                //Telefonnummern auslesen
                List<String> numbers = new ArrayList<>();
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                if(hasPhoneNumber > 0){
                    Cursor cursor2 = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[] {id},
                            null);

                    while(cursor2.moveToNext()){
                        String phoneNumber = cursor2.getString(cursor2.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //System.out.println("Contact: "+ name +" Nummer: "+ phoneNumber);
                        numbers.add(phoneNumber);
                    }
                    cursor2.close();
                }

                Contact contact;
                if(emails.size() > 0){
                    contact = new Contact(name, emails.get(0), numbers);
                }
                else{
                    contact = new Contact(name, "", numbers);
                }

                contactList.add(contact);
            }

            //Prüft ob gefundene Kontakte bereits in der Datenbank sind um diese nicht nocheinmal hinzuzufügen
            List<Contact> out = new ArrayList<>();
            List<Contact> currentContacts = contactAapter.getContacts();
            if (currentContacts.size() == 0){
                out = contactList;
            } else{
                for (Contact contactI: contactList) {
                    boolean found = false;
                    for (Contact contact: currentContacts) {
                        if (contact.equals(contactI)){
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        out.add(contactI);
                }
            }
            //Fügt alle neuen Kontakte in die Datenbank ein
            contactViewModel.insertAll(EncryptionManager.encryptData(out, context).stream().toArray(Contact[]::new));

        }
        cursor.close();

    }

}
