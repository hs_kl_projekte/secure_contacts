package com.hskl.secure_contacts;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

// Diese Klasse dient als zwischenschicht zwischen UI und Datenbank
// Das UI überwacht die hier zwischengeseicherten Daten und verwendet diese gegebenfalls weiter
public class ContactViewModel extends AndroidViewModel {

    private ContactRepository mRepository;
    private LiveData<List<Contact>> mAllContacts;
    private LiveData<Contact> mProfile;

    public ContactViewModel(@NonNull Application application) {
        super(application);
        mRepository = new ContactRepository(application);
        mAllContacts = mRepository.getAllContacts();
        mProfile = mRepository.getProfile();
    }

    LiveData<List<Contact>> getAllContacts(){
        return mAllContacts;
    }

    LiveData<Contact> getProfile(){
        return mProfile;
    }
    LiveData<List<Contact>> getContact(int contactID){ ;
        return mRepository.getContactsById(contactID);
    }

    public void insertAll(Contact... contacts){
        mRepository.insertAll(contacts);
    }

    public void delete(int contactID){
        mRepository.delete(contactID);
    }

    public void deleteAll(){
        mRepository.deleteAll();
    }

    public void updateContact(Contact contact){
        mRepository.updateContact(contact);
    }



}
