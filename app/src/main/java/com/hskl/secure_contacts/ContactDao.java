package com.hskl.secure_contacts;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;


// Hier werden die SQL ausdrücke mit den Funktionsaufrufen verknüpft
@Dao
public interface ContactDao {

    @Query("SELECT * FROM contacts WHERE isProfile = 0")
    LiveData<List<Contact>> getAllContacts();

    @Query("SELECT * FROM contacts WHERE mContactID IN (:contactIds) AND isProfile = 0")
    LiveData<List<Contact>> loadAllByIds(int... contactIds);

    @Query("SELECT * FROM contacts WHERE isProfile = 1")
    LiveData<Contact> getProfile();

    @Insert
    void insert(Contact... contacts);

    @Query("DELETE FROM contacts WHERE mContactID = :contactID")
    void delete(int contactID);

    @Query("DELETE FROM contacts")
    void deleteAll();

    @Update
    void updateContact(Contact contact);

}
