package com.hskl.secure_contacts;

import android.content.Context;
import android.renderscript.RenderScript;

import java.util.ArrayList;
import java.util.List;

//Verantwortlich für die Verschlüsselung der Kontaktdaten
public class EncryptionManager {

    private EncryptionManager(){}

    //Methode zum entschlüsseln der in der Datenbank gespeicherten Kontaktdaten
    protected static Contact decryptContact(Contact contact, Context context){
        if(contact != null){
            //Erstellen eines Cryptography Objekts um Zugriff auf den Keystore zu erhalten
            Cryptography cryptography = new Cryptography(context);

            //Entschlüsseln der einzelnen Einträge des Kontakts
            contact.setName(cryptography.decryptData(contact.getName()));
            List<String> decryptedNumbers = new ArrayList<>();
            for (String number: contact.getPhoneNumbers())
            {
                decryptedNumbers.add(cryptography.decryptData(number));
            }
            contact.setPhoneNumbers(decryptedNumbers);
            if (contact.getEmail() != null && !contact.getEmail().isEmpty()) contact.setEmail(cryptography.decryptData(contact.getEmail()));


            return contact;
        }
        else{
            return null;
        }


    }

    //Entschlüsselt eine Liste von Kontakten mit Hilfe der decryptContact Methode
    protected static List<Contact> decryptAllContacts(List<Contact> contacts, Context context){
        if(contacts != null){
            List<Contact> decryptedContacs = new ArrayList<>();
            for (Contact contact: contacts)
            {
                decryptedContacs.add(decryptContact(contact, context));
            }
            return decryptedContacs;
        }
        else{
            return null;
        }

    }
    //Verschlüsselt eine Liste von Kontakten mit Hilfe der encryptContact Methode
    protected static List<Contact> encryptData(List<Contact> contacts, Context context){
        if(contacts != null){
            List<Contact> encryptedContacts = new ArrayList<>();
            for (Contact contact: contacts)
            {
                encryptedContacts.add(encryptContact(contact, context));
            }

            return encryptedContacts;
        }
        else{
            return null;
        }

    }
    //Methode zum verschlüsseln eines Kontakts der in der Datenbank gespeichert werden soll
    protected static Contact encryptContact (Contact contact, Context context){
        if (contact != null){
            Cryptography cryptography = new Cryptography(context);

            contact.setName(cryptography.encryptData(contact.getName()));
            List<String> encryptedNumbers = new ArrayList<>();
            for (String number: contact.getPhoneNumbers())
            {
                encryptedNumbers.add(cryptography.encryptData(number));
            }
            contact.setPhoneNumbers(encryptedNumbers);
            if(contact.getEmail() != null && !contact.getEmail().isEmpty()) contact.setEmail(cryptography.encryptData(contact.getEmail()));

            return contact;
        }
        else{
            return null;
        }


    }


}
