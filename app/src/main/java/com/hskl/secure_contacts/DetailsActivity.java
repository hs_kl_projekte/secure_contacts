package com.hskl.secure_contacts;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    public static final String EXTRA_REPLY_DELETE = "DELETE_CONTACT";
    public static final String EXTRA_REPLY_CHANGE = "CHANGE_CONTACT";

    private Contact mContact;

    private LinearLayout mPhoneNumberHolder;
    private LinearLayout mDeleteMenu;
    private ImageView mBackButton;
    private Button mMenuButton;
    private TextView mDeleteButton;
    private TextView mName;
    private TextView mEmail;

    private boolean notChanged = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        mBackButton = findViewById(R.id.details_back_button);
        mBackButton.setOnClickListener(view -> {
            Intent replyIntent = new Intent();
            if (notChanged){
                setResult(RESULT_CANCELED, replyIntent);
            } else {
                replyIntent.putExtra("EXTRA_CONTACT", mContact);
                setResult(RESULT_OK, replyIntent);
            }
            finish();
        });

        mDeleteMenu = findViewById(R.id.details_menu_holder);
        mDeleteMenu.setVisibility(View.GONE);


        mMenuButton = findViewById(R.id.details_menu_button);
        mMenuButton.setOnClickListener(view -> {
            if (mDeleteMenu.getVisibility() == View.GONE){
                mDeleteMenu.setVisibility(View.VISIBLE);
            } else{
                mDeleteMenu.setVisibility(View.GONE);
            }
        });

        mDeleteButton = findViewById(R.id.details_delete_button);
        mDeleteButton.setOnClickListener(view -> {
            Intent replyIntent = new Intent();
            replyIntent.putExtra(EXTRA_REPLY_DELETE, mContact.getContactID());
            setResult(RESULT_FIRST_USER, replyIntent);
            finish();
        });

        mContact = getIntent().getParcelableExtra("EXTRA_CONTACT");

        mPhoneNumberHolder = findViewById(R.id.details_phone_number_holder);

        mName = findViewById(R.id.details_name_textView);
        mEmail = findViewById(R.id.details_email_textView);

        mName.setText(mContact.getName());
        mEmail.setText(mContact.getEmail());

        for(int i = 0; i < mContact.getPhoneNumbers().size(); i++){
            System.out.println(mContact.getPhoneNumbers().get(i));
            LinearLayout element = ((LinearLayout)getLayoutInflater().inflate(R.layout.details_element_layout, mPhoneNumberHolder));
            ConstraintLayout cl = (ConstraintLayout) element.getChildAt(i);
            TextView textView = (TextView) cl.getViewById(R.id.details_phone_number);
            textView.setText(mContact.getPhoneNumbers().get(i));

            final int iteration = i;
            ImageView callButton = (ImageView) cl.getViewById(R.id.details_phone_number_call_icon);
            callButton.setOnClickListener(view -> dialContact(mContact.getPhoneNumbers().get(iteration)));

            ImageView smsButton = (ImageView) cl.getViewById(R.id.details_phone_number_sms_icon);
            smsButton.setOnClickListener(view -> startMessage(mContact.getPhoneNumbers().get(iteration)));
        }
    }

    public void dialContact(final String phoneNumber){
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null)));
    }

    public void startMessage(final String phoneNumber){
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phoneNumber, null)));
    }
}
