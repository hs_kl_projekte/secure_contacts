package com.hskl.secure_contacts;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.Arrays;


// Die Datenbankverwalung
// Für diese Projekt habe wir Room verwendet, da nur einfache zugriffe auf die Datenbank nötig waren
@Database(entities = {Contact.class}, version = 3)
@TypeConverters({Converters.class})
public abstract class ContactsDatabase extends RoomDatabase {

    private static final String DB_NAME = "secureContacts.db";
    private static volatile ContactsDatabase instance;

    public static synchronized ContactsDatabase getInstance(final Context context){
        if (instance == null){
            synchronized (ContactsDatabase.class){
                if (instance == null){
                    instance = create(context.getApplicationContext());
                }
            }
        }
        return instance;
    }


    private static ContactsDatabase create(final Context context){

        return Room.databaseBuilder(context, ContactsDatabase.class, DB_NAME).fallbackToDestructiveMigration().build();
    }

    public abstract ContactDao contactDao();


    // Wird nur für Testzwecke verwendet
    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            // If you want to keep the data through app restarts,
            // comment out the following line.
            new PopulateDbAsync(instance).execute();
        }
    };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final ContactDao mDao;

        PopulateDbAsync(ContactsDatabase db) {
            mDao = db.contactDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            // Start the app with a clean database every time.
            // Not needed if you only populate on creation.
            mDao.deleteAll();

            //Encryption fehlt hier noch, deswegen auskommentiert
/*
            Contact contact = new Contact("Philipp Zimmermann", "phizi@nojunk.de", Arrays.asList("+4917642063876"));
            contact.setProfile(true);
            mDao.insert(contact);
            contact = new Contact("Test mop", "test@test.de", Arrays.asList("+6464654654"));
            mDao.insert(contact);
*/
            return null;
        }
    }

}
