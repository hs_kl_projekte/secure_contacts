package com.hskl.secure_contacts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {

    private Context mContext;
    private Contact mProfile;
    private List<Contact> mDataset = Collections.emptyList();
    private View mActiveView;

    public static class ContactViewHolder extends RecyclerView.ViewHolder {


        public ViewGroup viewGroup;

        public ContactViewHolder(ViewGroup v){
            super(v);
            viewGroup = v;
        }
    }

    public ContactAdapter(Context context){
        mContext = context;

    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewGroup v;
        if (viewType == 1){
            v = (ViewGroup) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.profile_view, parent, false);
        } else {
            v = (ViewGroup) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_view, parent, false);
        }

        ContactViewHolder vh = new ContactViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
        if (mDataset != null) {
            if (position == 0 && mProfile != null) {
                TextView profileName = holder.viewGroup.findViewById(R.id.profile_name_textView);
                TextView profileNumber = holder.viewGroup.findViewById(R.id.profile_number_textView);

                profileName.setText(mProfile.getName());
                profileNumber.setText(mProfile.getPhoneNumbers().get(0));

                return;
            }
            int adjPos = position - ((mProfile != null) ? 1 : 0 );
            TextView contactName = holder.viewGroup.findViewById(R.id.contact_name_textView);
            TextView contactLetter = holder.viewGroup.getChildAt(0).findViewById(R.id.contact_image_textView);

            contactName.setText(mDataset.get(adjPos).getName());
            contactLetter.setText(mDataset.get(adjPos).getName().toUpperCase().substring(0, 1));

            final View buttonHolder = holder.viewGroup.findViewById(R.id.contacts_extra_button_holder);
            buttonHolder.setVisibility(GONE);

            View buttonHolderConstraint = ((ViewGroup) buttonHolder).getChildAt(0);
            View callButton = buttonHolderConstraint.findViewById(R.id.callLayout);
            callButton.setOnClickListener(v -> dialContact(mDataset.get(adjPos).getFavouritePhoneNumber()));

            View messageButton = buttonHolderConstraint.findViewById(R.id.messageLayout);
            messageButton.setOnClickListener(v -> startMessage(mDataset.get(adjPos).getFavouritePhoneNumber()));

            View emailButton = buttonHolderConstraint.findViewById(R.id.emailLayout);
            emailButton.setOnClickListener(v -> startEmail(mDataset.get(adjPos).getEmail()));

            View detailsButton = buttonHolderConstraint.findViewById(R.id.detailsLayout);
            detailsButton.setOnClickListener(v -> openDetails(mDataset.get(adjPos).getContactID()));

            holder.viewGroup.findViewById(R.id.contact_info_holder).setOnClickListener(v -> toggleExtraButtons(buttonHolder));
        }

    }

    @Override
    public int getItemCount() {
        if (mDataset != null)
            return mDataset.size() + ((mProfile != null) ? 1 : 0 );
        else return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && mProfile != null)
            return 1;
        else
            return 2;
    }

    public void setProfile(Contact contact){
        mProfile = contact;
        notifyDataSetChanged();
    }

    public void setContacts(List<Contact> contacts){
        contacts.sort(Comparator.comparing(Contact::getName));
        mDataset = contacts;
        notifyDataSetChanged();
    }

    public List<Contact> getContacts(){
        return mDataset;
    }

    private void toggleExtraButtons(View view){
        if (view.getVisibility() == GONE){
            if (mActiveView != null)
                mActiveView.setVisibility(GONE);
            view.setVisibility(VISIBLE);
            mActiveView = view;
        } else {
            view.setVisibility(GONE);
        }
    }

    private void dialContact(final String phoneNumber){
        ((MainActivity) mContext).dialContact(phoneNumber);
    }

    private void startMessage(final String phoneNumber){
        ((MainActivity) mContext).startMessage(phoneNumber);
    }

    private void startEmail(final String email){
        ((MainActivity) mContext).startEmail(email);

    }

    private void openDetails(final int contactID){
        ((MainActivity) mContext).openDetails(contactID);
    }



}
