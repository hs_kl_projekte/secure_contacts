package com.hskl.secure_contacts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Arrays;

// Diese Activity handelt das Erstellen eines neuen Kontakts
public class NewContactActivity extends AppCompatActivity {

    public static final String EXTRA_REPLY = "com.hskl.android.contactlistsql.REPLY";

    private EditText mName;
    private EditText mWork;
    private EditText mPhone;
    private EditText mEmail;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_contact);

        mName = findViewById(R.id.new_contact_name_editText);
        mWork = findViewById(R.id.new_contact_work_editText);
        mPhone = findViewById(R.id.new_contact_phone_editText);
        mEmail = findViewById(R.id.new_contact_email_editText);

        final TextView saveButton = findViewById(R.id.saveButton);
        saveButton.setOnClickListener(view -> {
            Intent replyIntent = new Intent();
            if (TextUtils.isEmpty(mName.getText())){
                setResult(RESULT_CANCELED, replyIntent);
            } else {
                String[] contactData = new String[] {mName.getText().toString(), mWork.getText().toString(), mPhone.getText().toString(), mEmail.getText().toString()};
                replyIntent.putExtra(EXTRA_REPLY, contactData);
                setResult(RESULT_OK, replyIntent);
            }
            finish();
        });

        final TextView cancleButton = findViewById(R.id.cancleButton);
        cancleButton.setOnClickListener(view -> {
            Intent replyIntent = new Intent();
            setResult(RESULT_FIRST_USER, replyIntent);
            finish();
        });


    }
}
