package com.hskl.secure_contacts;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    public static final int NEW_CONTACT_ACTIVITY_REQUEST_CODE = 1;
    public static final int DETAILS_ACTIVITY_REQUEST_CODE = 2;

    private ContactViewModel mContactViewModel;

    private RecyclerView recyclerView;
    private ContactAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;


    private ImageView mMenuButton;
    private LinearLayout mMenuHolder;
    private TextView mDeleteButton;
    private TextView mImportButton;


    // Buttons und andere UI Elemente werden hier gebunden
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mMenuButton = findViewById(R.id.main_menu_button);
        mMenuHolder = findViewById(R.id.main_menu_holder);
        mMenuHolder.setVisibility(View.GONE);
        mDeleteButton = findViewById(R.id.main_delete_button);
        mImportButton = findViewById(R.id.main_import_button);
        recyclerView = findViewById(R.id.main_contacts_recyclerView);

        mMenuButton.setOnClickListener(view -> {
            if (mMenuHolder.getVisibility() == View.VISIBLE){
                mMenuHolder.setVisibility(View.GONE);
            } else {
                mMenuHolder.setVisibility(View.VISIBLE);
            }
        });

        mDeleteButton.setOnClickListener(view -> {
            mContactViewModel.deleteAll();
            mMenuHolder.setVisibility(View.GONE);
        });

        mImportButton.setOnClickListener(view -> {
            ContactManager.requestContactPermission(this);
            mMenuHolder.setVisibility(View.GONE);
        });




        // Erzeugt den Adapter for die RecyclerView
        mAdapter = new ContactAdapter(this);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(false);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);


        mContactViewModel = ViewModelProviders.of(this).get(ContactViewModel.class);
        //mContactViewModel.getProfile().observe(this, contact -> mAdapter.setProfile(contact));
        //mContactViewModel.getAllContacts().observe(this, contacts -> mAdapter.setContacts(contacts));

        mContactViewModel.getProfile().observe(this, contact -> mAdapter.setProfile(EncryptionManager.decryptContact(contact, this)));
        mContactViewModel.getAllContacts().observe(this, contacts -> mAdapter.setContacts(EncryptionManager.decryptAllContacts(contacts, this)));


        // Cutsom FAB da der Standard FAB verbugt war
        ImageView fab = findViewById(R.id.new_contact_fab);
        fab.setOnClickListener(view -> {

            Intent intent = new Intent(MainActivity.this, NewContactActivity.class);
            startActivityForResult(intent, NEW_CONTACT_ACTIVITY_REQUEST_CODE);
        });

    }

    // Wartet auf die Permission für das Importieren
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == 1){
            ContactManager.importContacts(this, mContactViewModel, mAdapter);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();


        // specify an adapter
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Wartet auf verschiedene Rückmeldungen von gestarteten Activities
    // und verarbeitet diese dann weiter
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_CONTACT_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK){
            String[] replyData = data.getStringArrayExtra(NewContactActivity.EXTRA_REPLY);
            Contact contact = new Contact(replyData[0], replyData[3], Arrays.asList(replyData[1], replyData[2]));
            mContactViewModel.insertAll(EncryptionManager.encryptContact(contact, this));
        } else if (requestCode == NEW_CONTACT_ACTIVITY_REQUEST_CODE && resultCode == RESULT_CANCELED){
            Toast.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }

        if (requestCode == DETAILS_ACTIVITY_REQUEST_CODE && resultCode == RESULT_FIRST_USER){
            int replyData = data.getIntExtra(DetailsActivity.EXTRA_REPLY_DELETE, -1);
            mContactViewModel.delete(replyData);
        } else if (requestCode == DETAILS_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK){
            Contact contactToUpdate = data.getParcelableExtra("EXTRA_CONTACT");
            mContactViewModel.updateContact(contactToUpdate);
        }
    }

    // Startet die Activity zum Anrufen
    public void dialContact(final String phoneNumber){
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null)));
    }

    // Startet die Activity zum SMS versenden
    public void startMessage(final String phoneNumber){
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phoneNumber, null)));
    }

    // Startet die Activity zum Email versenden

    public void startEmail(final String email){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri data = Uri.parse("mailto:" + email+ "?subject=");
        intent.setData(data);
        startActivity(intent);
    }

    private boolean hasDetails = false;

    // Öffnet den Details Screen, muss überprüfen, ob der alte Observer zerstört ist
    public void openDetails(final int contactID){
        if (hasDetails){
            mContactViewModel.getContact(contactID).removeObservers(this);
            hasDetails = false;
        }
        mContactViewModel.getContact(contactID).observe(this, contacts -> {

            if (!hasDetails){
                Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                intent.putExtra("EXTRA_CONTACT", EncryptionManager.decryptContact(contacts.get(0), this));
                startActivityForResult(intent, DETAILS_ACTIVITY_REQUEST_CODE);
                hasDetails = true;
            }
        });
    }

}
